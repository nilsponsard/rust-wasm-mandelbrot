extern crate wasm_bindgen;
extern crate web_sys;
use wasm_bindgen::prelude::*;

#[derive(Copy, Clone)]
pub struct Color {
    r: u8,
    g: u8,
    b: u8,
}
impl Color {
    // pub fn clone(&self) -> Color {
    //     Color {
    //         r: self.r,
    //         g: self.g,
    //         b: self.b,
    //     }
    // }
}

fn get_portion(i: u16, value1: u8, value2: u8, max: usize) -> u8 {
    let out: u8 = (value1 as f64 + ((value2 as f64 - value1 as f64) * i as f64 / max as f64)) as u8;
    // web_sys::console::log_1(&wasm_bindgen::JsValue::from_f64((out as f64)));
    return out;
}

#[wasm_bindgen]
pub struct MandelbrotViewport {
    width: f64,
    height: f64,
    center_x: f64,
    center_y: f64,
    target_width: u32,
    target_height: u32,
    max_i: u16,
    // color_set: Vec<Color>,
    data_array: Vec<u8>,
}
#[wasm_bindgen]
impl MandelbrotViewport {
    pub fn new(
        width: f64,
        height: f64,
        center_x: f64,
        center_y: f64,
        target_width: u32,
        target_height: u32,
    ) -> MandelbrotViewport {
        let data_array = (0..target_width * target_height * 4)
            .map(|_i| 255)
            .collect();
        // let mut color_set: Vec<Color> = vec![];
        // let mut points: Vec<Color> = vec![];
        // // color_set.push(Color { r: 0, g: 7, b: 100 });
        // points.push(Color { r: 0, g: 7, b: 100 });
        // points.push(Color {
        //     r: 32,
        //     g: 107,
        //     b: 203,
        // });
        // points.push(Color {
        //     r: 237,
        //     g: 255,
        //     b: 255,
        // });
        // points.push(Color {
        //     r: 255,
        //     g: 170,
        //     b: 0,
        // });
        // points.push(Color { r: 0, g: 2, b: 0 });
        // let mut r: u8;
        // let mut g: u8;
        // let mut b: u8;
        // let points_length = points.len() - 1;
        // let portion = 100;
        // let palette_length = portion * points_length;

        // for i in 0..palette_length {
        //     let index = (i * points_length) / palette_length + 1;
        //     r = get_portion(
        //         i as u16,
        //         points[index - 1].r,
        //         points[index].r,
        //         palette_length,
        //     );
        //     g = get_portion(
        //         i as u16,
        //         points[index - 1].g,
        //         points[index].g,
        //         palette_length,
        //     );
        //     b = get_portion(
        //         i as u16,
        //         points[index - 1].b,
        //         points[index].b,
        //         palette_length,
        //     );

        //     color_set.push(Color { r, g, b });
        // }
        // color_set.push(points[points.len() - 1]);
        MandelbrotViewport {
            width,
            height,
            center_x,
            center_y,
            target_width,
            target_height,
            max_i: 100,
            // color_set,
            data_array,
        }
    }

    pub fn zoom(&mut self, multiply: f64) {
        let mut n_multiply = multiply;
        if n_multiply < 0.1 {
            n_multiply = 0.1;
        }
        self.width = self.width * (1.0 / n_multiply);
        self.height = self.height * (1.0 / n_multiply);
        // self.offset_x = self.offset_x * ((1.0 / multiply) - 1.0);
        // self.offset_y = self.offset_y * ((1.0 / multiply) - 1.0);
        // self.offset_x = self.offset_x * (1.0 / multiply);
        // self.offset_y = self.offset_y * (1.0/ multiply);
    }
    pub fn scale_x(&self, x: i32) -> f64 {
        (x as f64) / (self.target_width as f64) * self.width
    }
    pub fn scale_y(&self, y: i32) -> f64 {
        (y as f64) / (self.target_height as f64) * self.height
    }
    pub fn transform_x(&self, x: i32) -> f64 {
        self.scale_x(x) + self.center_x - self.width / 2.0
    }
    pub fn transform_y(&self, y: i32) -> f64 {
        self.scale_y(y) + self.center_y - self.width / 2.0
    }

    pub fn move_delta(&mut self, delta_x: i32, delta_y: i32) {
        let dx = self.scale_x(delta_x);
        let dy = self.scale_y(delta_y);
        self.center_x += dx;
        self.center_y += dy;
    }

    fn get_level_at(&self, x: i32, y: i32) -> u16 {
        self.get_level(self.transform_x(x), self.transform_y(y))
    }
    pub fn get_x(&self) -> f64 {
        self.center_x
    }
    pub fn get_y(&self) -> f64 {
        self.center_y
    }
    pub fn set_x(&mut self, x: f64) {
        self.center_x = x;
    }
    pub fn set_y(&mut self, y: f64) {
        self.center_y = y;
    }
    pub fn get_width(&self) -> f64 {
        self.width
    }
    pub fn get_height(&self) -> f64 {
        self.height
    }
    pub fn get_target_width(&self) -> u32 {
        self.target_width
    }
    pub fn get_target_height(&self) -> u32 {
        self.target_height
    }
    pub fn draw(&mut self) {
        for x in 0..self.target_width {
            for y in 0..self.target_height {
                let index = ((y * self.target_width + x) * 4) as usize;
                let l = self.get_level_at(x as i32, y as i32);
                let level = ((l as f64 * 255.0) / self.max_i as f64) as u8;
                self.data_array[index + 0] = level;
                self.data_array[index + 1] = level;
                self.data_array[index + 2] = level;
            }
        }
    }
    pub fn get_array(&self) -> *const u8 {
        self.data_array.as_ptr()
    }
    fn get_level(&self, origin_a: f64, origin_b: f64) -> u16 {
        let mut a = origin_a;
        let mut b = origin_b;

        let mut n = 0;
        let mut aa: f64;
        let mut bb: f64;
        while n < self.max_i && a * a + b * b < 4.0 {
            aa = a * a - b * b;
            bb = 2.0 * a * b;
            a = aa + origin_a;
            b = bb + origin_b;
            n += 1;
        }
        // let smoothed = ((a * a + b * b).log2() / 2.0).log2(); // log_2(log_2(|p|))
        // let mut color_i = (((n as f64 + 10.0 - smoothed).sqrt() * 256.0) as i32
        //     % self.color_set.len() as i32) as usize;
        // let mut color_i = (n as usize * self.color_set.len()) / self.max_i as usize;
        // if color_i >= self.color_set.len() {
        //     color_i = self.color_set.len() - 1;
        // }

        return n;
    }
    pub fn get_max_i(&self) -> u16 {
        self.max_i
    }
    pub fn set_max_i(&mut self, n: u16) {
        self.max_i = n;
    }
}
