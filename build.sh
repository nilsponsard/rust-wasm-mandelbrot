curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs > rust-install.sh
sh rust-install.sh -y
source $HOME/.cargo/env
curl https://rustwasm.github.io/wasm-pack/installer/init.sh -sSf | sh 
wasm-pack build --release 
cd www
npm install
echo "compiling with tsc"
npx tsc
ls -al 
npm run-script build
mv dist ../public
